import groovy.json.JsonSlurper;

def jsonSlurper = new JsonSlurper()
//def blueprint = args[0]
//def rig_url = args[1]

//def obj = jsonSlurper.parseText(blueprint)

//def riglet_name = obj.riglet_info.name
//def lob = obj.component_info.LOB
//def cfu = obj.component_info.CFU
//def app_name = obj.component_info.application_name
//def app_id = obj.component_info.application_Id
//def component_name = obj.component_info.component_name
//def repoId = obj.tool_info.gitlab_repo_Id

def lob = "ROBT"
def cfu = "B2B"
def app_name = "BBAC"
def app_id = "APP02906"
def component_name = "XML-Address"
def repoId = 31339
def riglet_name = "Tycoon_1235"

//rig_url = rig_url + "/api/riglets/connectorServerDetails"  // fetching tool details from rig
//def postGit = new URL(rig_url).openConnection();
def message = ""
//def message = '{"rigletName": "' +riglet_name +'","toolName": "gitlab"}'
//postGit.setRequestMethod("POST")
//postGit.setDoOutput(true)
//postGit.setRequestProperty("Content-Type", "application/json")
//postGit.getOutputStream().write(message.getBytes("UTF-8"));
//def postGitRC = postGit.getResponseCode();

//def gitDetails = ""
//if(postGitRC.equals(200)) {
//    gitDetails = postGit.getInputStream().getText()
//}

//gitDetails = jsonSlurper.parseText(gitDetails)   // fetching tool details from rigJson
//def url = gitDetails.url
//def username = gitDetails.username
//def password = gitDetails.password

def username = "*********"
def password = "*****************"
def gitlabUrl = "*******************************"


def today = new Date()
def currentTime = today.format("yyyy-MM-dd HH:mm:ss",TimeZone.getTimeZone('IST')) // provides current time
def yesterday = today.previous().format("yyyy-MM-dd"); // provides previous date

def repoDetail = "$gitlabUrl/api/v4/projects/$repoId" // api to fetch repo details from repo Name
def getRepoDetail = new URL(repoDetail).openConnection();
getRepoDetail.setRequestMethod("GET")
getRepoDetail.setDoOutput(true)
getRepoDetail.setRequestProperty("Content-Type", "application/json")
getRepoDetail.setRequestProperty("PRIVATE-TOKEN", password)

def repoJson = ""
def getRepoRC = getRepoDetail.getResponseCode();

if(getRepoRC.equals(200)) {
    repoJson = getRepoDetail.getInputStream().getText()

}

repoJson = jsonSlurper.parseText(repoJson)

def repoName = repoJson.name
def contributor = ""
def getContriDetail = ""
def contriJson = ""
def getContriRC = 0
def contriSize = 0
def branches = ""
def getBranchDetail = ""
def branchJson = ""
def getBranchRC = 0
def branchSize = 0
def branchName = ""
def commits = ""
def getCommitDetail = ""
def commitJson = ""
def getCommitRC = 0
def commitSize = 0
def committer_name = ""
def committer_email = ""
def committed_date = ""
def commit_id = ""
def post = ""
def postRC = ""
def pageNo = 1

// below mentioned block is how we call a shell script inside a groovy script
contributor = "$gitlabUrl/api/v4/projects/$repoId/repository/contributors?pagination=keyset&per_page=100" // api to fetch list of contributors
getContriDetail = new URL(contributor).openConnection();
getContriDetail.setRequestMethod("GET")
getContriDetail.setDoOutput(true)
getContriDetail.setRequestProperty("Content-Type", "application/json")
getContriDetail.setRequestProperty("PRIVATE-TOKEN", password)

getContriRC = getContriDetail.getResponseCode();
if(getContriRC.equals(200)) {
    contriJson = getContriDetail.getInputStream().getText()

}

contriJson = jsonSlurper.parseText(contriJson)
contriSize = contriJson.size()

int j =0;
//defining array of string
def String[] contriNames = new String[contriSize]
while(j < contriSize)
{
contriNames[j]= "\"" + contriJson[j].email + "\""; // adding list of contributors to the array
j++
}
//print(contriNames)

branches = "$gitlabUrl/api/v4/projects/$repoId/repository/branches?pagination=keyset&page=1&per_page=100" // api to fetch list of branches
getBranchDetail = new URL(branches).openConnection();
getBranchDetail.setRequestMethod("GET")
getBranchDetail.setDoOutput(true)
getBranchDetail.setRequestProperty("Content-Type", "application/json")
getBranchDetail.setRequestProperty("PRIVATE-TOKEN", password)

getBranchRC = getBranchDetail.getResponseCode();
//println(getBranchRC);
if(getBranchRC.equals(200)) {
    branchJson = getBranchDetail.getInputStream().getText()

}

branchJson = jsonSlurper.parseText(branchJson)
branchSize = branchJson.size()

int i = 0;


while( i < branchSize) // iterating over the
  {

    branchName=branchJson[i].name;
    def branch = branchName.replace("+","%2B")
    pageNo = 1
    while(pageNo != 0)
    {
    commits = "$gitlabUrl/api/v4/projects/$repoId/repository/commits?ref_name=$branch&since=2020-09-30T00:00:00Z&pagination=keyset&page=$pageNo&per_page=100"  // api to fetch commit details for a particular branch
    getCommitDetail = new URL(commits).openConnection();
    getCommitDetail.setRequestMethod("GET")
    getCommitDetail.setDoOutput(true)
    getCommitDetail.setRequestProperty("Content-Type", "application/json")
    getCommitDetail.setRequestProperty("PRIVATE-TOKEN", password)


    getCommitRC = getCommitDetail.getResponseCode();
    //println(getCommitRC);
    if(getCommitRC.equals(200)) {
        commitJson = getCommitDetail.getInputStream().getText()
    }

    commitJson = jsonSlurper.parseText(commitJson)
    commitSize = commitJson.size()
    if(commitSize == 0)
    {
        pageNo = 0
    }
    else
    {
        pageNo = pageNo + 1

    int l=0;
    while(l < commitSize)
    {
        committer_name = commitJson[l].committer_name
        committer_email = commitJson[l].committer_email
        committed_date = commitJson[l].committed_date
        commit_id = commitJson[l].id
        if(pageNo % 3 == 0 )
        {
            pushToPostgresThree( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )  // function calling
        }
        else if(pageNo % 2 == 0)
        {
            pushToPostgresTwo( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )  // function calling
        }
        else
        {
            pushToPostgresOne( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )  // function calling
        }
    l++;
    }
    if(commitSize < 100)
        {
          pageNo = 0
        }
    }
  //  Thread.sleep(2000);
  }
i++;
//Thread.sleep(5000);
  }
//   viewData()  // calling the viewData function
 //}
 //else
 //{
 //    println(" Repository name not matching Please Check!!! ");
 //}
//r++;
//}

// postGres function for pushing the raw data
def pushToPostgresOne( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )
{

    post = new URL("http://10.9.137.169:49544/gitlab_raw").openConnection();
    message = '{ ' + ' "lob": "' + lob + '", "cfu": "' + cfu + '", "application_name": "' + app_name + '", "application_id": "' + app_id + '", "component_name": "' + component_name + '", "repo_name": "' + repoName + '", "branch_name": "' + branchName + '", "time": "' + currentTime + '", "committer_name": "' + committer_name + '", "committer_email": "' + committer_email + '", "committed_date": "' + committed_date + '", "contributorlist": ' + contriNames + ',"commit_id": "' + commit_id + '", "riglet_name": "' + riglet_name + '","repo_id": "' + repoId + '" }'
//    println (message);
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(message.getBytes("UTF-8"));
    postRC = post.getResponseCode();
    println(postRC);
    if(postRC.equals(200)) {
    println(post.getInputStream().getText());
     }
}

def pushToPostgresTwo( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )
{

    post = new URL("http://10.36.69.205:61114/gitlab_raw").openConnection();
    message = '{ ' + ' "lob": "' + lob + '", "cfu": "' + cfu + '", "application_name": "' + app_name + '", "application_id": "' + app_id + '", "component_name": "' + component_name + '", "repo_name": "' + repoName + '", "branch_name": "' + branchName + '", "time": "' + currentTime + '", "committer_name": "' + committer_name + '", "committer_email": "' + committer_email + '", "committed_date": "' + committed_date + '", "contributorlist": ' + contriNames + ',"commit_id": "' + commit_id + '", "riglet_name": "' + riglet_name + '","repo_id": "' + repoId + '" }'
 //   println (message);
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(message.getBytes("UTF-8"));
    postRC = post.getResponseCode();
    println(postRC);
    if(postRC.equals(200)) {
    println(post.getInputStream().getText());
     }
}

def pushToPostgresThree( lob,cfu,app_name,app_id,component_name,repoName,repoId,branchName,currentTime,committer_name, committer_email, committed_date,contriNames,commit_id,riglet_name,post,message,postRC )
{

    post = new URL("http://10.36.69.103:61116/gitlab_raw").openConnection();
      message = '{ ' + ' "lob": "' + lob + '", "cfu": "' + cfu + '", "application_name": "' + app_name + '", "application_id": "' + app_id + '", "component_name": "' + component_name + '", "repo_name": "' + repoName + '", "branch_name": "' + branchName + '", "time": "' + currentTime + '", "committer_name": "' + committer_name + '", "committer_email": "' + committer_email + '", "committed_date": "' + committed_date + '", "contributorlist": ' + contriNames + ',"commit_id": "' + commit_id + '", "riglet_name": "' + riglet_name + '","repo_id": "' + repoId + '" }'
 //   println (message);
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(message.getBytes("UTF-8"));
    postRC = post.getResponseCode();
    println(postRC);
    if(postRC.equals(200)) {
    println(post.getInputStream().getText());
     }
}

// def get = ""
// def getRC = 0
// // viewData function for providing the table details
// def viewData() {
// get = new URL("http://13.58.47.71:3423/gitlab_raw").openConnection();
// getRC = get.getResponseCode();
// println(getRC);
// if(getRC.equals(200)) {
//     println(get.getInputStream().getText());
// }
// }

