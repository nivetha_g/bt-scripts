import java.text.SimpleDateFormat;
import groovy.json.JsonSlurper;
import java.util.concurrent.TimeUnit;

//def blueprint = args[0]
//def rig_url = args[1]

def jsonSlurper = new JsonSlurper() // using JsonSlurper function to read the json
//def obj = jsonSlurper.parseText(blueprint)

//def riglet_name = obj.riglet_info.name
//def lob = obj.component_info.LOB
//def cfu = obj.component_info.CFU
//def app_name = obj.component_info.application_name
//def component_name = obj.component_info.component_name
//def pipelineURL = obj.tool_info.jenkins_pipeline_URL

//rig_url = rig_url + "/api/riglets/connectorServerDetails"  // fetching tool details from rig
//def postJenkins = new URL(rig_url).openConnection();
//def message = '{"rigletName": "' +riglet_name +'","toolName": "jenkins"}'
//postJenkins.setRequestMethod("POST")
//postJenkins.setDoOutput(true)
//postJenkins.setRequestProperty("Content-Type", "application/json")
//postJenkins.getOutputStream().write(message.getBytes("UTF-8"));
//def postGitRC = postJenkins.getResponseCode();

//def jenkinsDetails = ""
//if(postGitRC.equals(200)) {
//    jenkinsDetails = postJenkins.getInputStream().getText()
//}

//jenkinsDetails = jsonSlurper.parseText(jenkinsDetails)   // fetching tool details from rigJson
//def url = jenkinsDetails.url
//def username = jenkinsDetails.username
//def password = jenkinsDetails.password

def username = "**********"
def password = "**************" 
def pipelineURL = "*********************************************"

def riglet_name = "Tycoon_1234"
def lob = "ROBT"
def cfu = "B2B"
def app_name = "BBAC"
def app_id = "BBAC"
def component_name = "TOTL-Display-Service"
                  
def today = new Date()
def currentTime = today.format("yyyy-MM-dd HH:mm:ss",TimeZone.getTimeZone('IST'))

def jobDetail = ['sh','-c',"curl -u $username:$password '$pipelineURL/api/json?tree=name,jobs%5Bname%2CallBuilds%5Bnumber%2Cstatus%2Ctimestamp%2Cid%2Cresult%2Cduration%5D%5D' "].execute() // api to fetch repo details from repo Name
jobDetail.waitFor()
println jobDetail.err.text

def jobJson = jsonSlurper.parseText(jobDetail.text) // parsing the json file

int i = 0;
def pipeline_Name = jobJson.name

if(jobJson._class == "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject")
{
    while(i < jobJson.jobs.size() )
    { 
            int j = 0;
            def job_name = jobJson.jobs[i].name;
            if ( jobJson.jobs[i].allBuilds.size() != 0 )
            {
                while(j < jobJson.jobs[i].allBuilds.size())
                {
                    def build_number = jobJson.jobs[i].allBuilds[j].number
                    def build_result = jobJson.jobs[i].allBuilds[j].result
                    def buildduration = jobJson.jobs[i].allBuilds[j].duration

                    String build_duration = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(buildduration),
                    TimeUnit.MILLISECONDS.toMinutes(buildduration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(buildduration)),
                    TimeUnit.MILLISECONDS.toSeconds(buildduration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(buildduration)));

                    def build_date = jobJson.jobs[i].allBuilds[j].timestamp
                    def date = new Date( build_date )
                    build_date = new  SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).format( date )

                    def job_creation = jobJson.jobs[i].allBuilds[jobJson.jobs[i].allBuilds.size() - 1].timestamp;
                    def jobdate = new Date( job_creation )
                    job_creation = new  SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).format( jobdate )

                    
                    pushtoPostgres(lob,cfu,app_name,app_id,component_name,riglet_name,pipeline_Name,job_name,build_number,build_result,build_duration,build_date,job_creation,currentTime)
                j++;
                }
            }
            // else
            // {

            // def nullJobDetail = ['sh','-c',"curl -u $username:$password '$pipelineURL/job/$job_name/jobConfigHistory/api/json' "].execute() // api to fetch repo details from repo Name
            // nullJobDetail.waitFor()
            // println nullJobDetail.err.text
            // def nullJobJson = jsonSlurper.parseText(nullJobDetail.text) 

            //         def build_number = "NA"
            //         def build_result = "NA"
            //         def buildduration = "NA"
            //         def build_date = "NA"
            //         def job_creation = nullJobJson.jobConfigHistory[nullJobJson.jobConfigHistory.size() - 1].date;
            //         job_creation = job_creation.replace("_" , " ")
            //         println(job_creation + " " + job_name)
            //         pushtoPostgres(lob,cfu,app_name,component_name,riglet_name,pipeline_Name,job_name,build_number,build_result,build_duration,build_date,job_creation,currentTime)
            // }
    i++;
    }
}
else
{
    println("Not a multibranch pipeline");
}


def pushtoPostgres(lob,cfu,app_name,app_id,component_name,riglet_name,pipeline_name,job_name,build_number,build_result,build_duration,build_date,job_creation,currentTime) { // for pushing data to postgres
    def post = new URL("http://10.9.137.169:49544/jenkins_raw").openConnection();
    def message = '{ ' + ' "lob": "' + lob + '", "cfu": "' + cfu + '", "application_name": "' + app_name + '", "application_id": "' + app_id + '", "component_name": "' + component_name + '","riglet_name": "' + riglet_name + '","pipeline_name": "' + pipeline_name + '","job_name": "' + job_name + '","build_number": ' + build_number + ',"build_result": "' + build_result + '","build_duration": "' + build_duration + '","build_date": "' + build_date + '","job_creation_date": "' + job_creation + '","time": "' + currentTime +'"}'
   // println (message);
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(message.getBytes("UTF-8"));
    def postRC = post.getResponseCode();
    println(postRC);
    if(postRC.equals(200)) {
    println(post.getInputStream().getText());
     }
}
